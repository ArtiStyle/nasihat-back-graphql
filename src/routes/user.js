import express from 'express';
import multer from 'multer';

import { User } from '../schema/User';
import { Blog } from '../schema/Blog';

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/img');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});
const upload = multer({ storage });

const router = express.Router();

router.get('/', async (req, res) => {
  console.log("api")
  try {
    const users = await User.getUsers();
    res.send(users);
  } catch (e) {
    res.send(e);
  }
});
router.get('/user/:id', async (req, res) => {
  console.log("api")
  try {
    const users = await User.getUser(req.params.id);
    res.send(users);
  } catch (e) {
    res.send(e);
  }
});

router.post('/add', upload.single('avatar'), async (req, res) => {
  console.log(req.body);
  const defaultPath = { path: '/public/img/avatar.png' };
  const { file, body } = req || {};
  const { name, biography } = body || {};
  const { kz, ru } = biography || {};
  const { path } = file || defaultPath;

  try {
    const user = await User.createRecord({
      name,
      biography:{ kz, ru },
      avatar: path,
    });
    res.send(user);
  } catch (e) {
    res.send(e);
  }
});

router.post('/edit', upload.single('avatar'), async (req, res) => {
  console.log(req.body);
  const { file, body } = req || {};
  // const { _id, name, city, sex, email, phone, password } = body || {};
  const { path } = file || {};
  const object = body;
  if (path) {
    object.avatar = path;
  }

  try {
    const user = await User.editRecord(object);
    res.send(user);
  } catch (e) {
    res.send(e);
  }
});

router.post('/delete', async (req, res) => {
  const { _id } = req.body;
  try {
    const makal = await User.deleteRecord(_id);
    res.send(makal);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
