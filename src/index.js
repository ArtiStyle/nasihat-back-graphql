// @flow

import express from 'express';
// $FlowFixMe
import graphqlHTTP from 'express-graphql';
// $FlowFixMe
import bodyParser from 'body-parser';
// $FlowFixMe
import cookieParser from 'cookie-parser';
// $FlowFixMe
import morgan from 'morgan';
import schema from './schema/schema';
import cors from 'cors';

import DB from './schema/db';

DB.init();

const PORT = process.env.PORT || 8090;
const app = express();
// $FlowFixMe
app.use(cors())
app.use(express.static('./'));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(morgan('tiny'));
// app.get('/', (req, res) => {
//   res.send('./public');
// });

// $FlowFixMe
// app.use('/main', (req, res) => {
//   res.send(req.user);
// });

app.use(
  '/api',
  graphqlHTTP(() => {
    return {
      schema,
      graphiql: true,
    };
  })
);
app.use(require('./routes'));

app.listen(PORT, console.log(`App works on ${PORT}...`)); // eslint-disable-line no-console

export default app;
