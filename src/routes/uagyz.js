import express from 'express';

import { Uagyz } from '../schema/Uagyz';

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const questions = await Uagyz.getUagyzs();
    res.send(questions);
  } catch (e) {
    res.send(e);
  }
});
router.post('/', async (req, res) => {
  const { body } = req;
  console.log(body)
  const { titlekz, titleru, mainTextkz, mainTextru, author} = body;
  try {
    const question = await Uagyz.createRecord({ 
    title: {
      kz: titlekz,
      ru: titleru,
    },
    mainText: {
      kz: mainTextkz,
      ru: mainTextru,
    },
    author
     });
    res.send(question);
  } catch (e) {
    res.send(e);
  }
});
router.get('/:id', async (req, res) => {
  try {
    const question = await Uagyz.getUagyz(req.params.id);
    res.send(question);
  } catch (e) {
    res.send(e);
  }
});
router.post('/edit', async (req, res) => {
  const { body } = req || {};
  try {
    const question = await Uagyz.editRecord(body);
    res.send(question);
  } catch (e) {
    res.send(e);
  }
});

router.post('/delete', async (req, res) => {
  const { _id } = req.body;
  try {
    const makal = await Uagyz.deleteRecord(_id);
    res.send(makal);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
