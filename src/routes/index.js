import express from 'express';

const router = express.Router();

router.use('/apis/users', require('./user'));
router.use('/apis/blogs', require('./blog'));
router.use('/apis/questions', require('./question'));
router.use('/apis/uagyzs', require('./uagyz'));

module.exports = router;
