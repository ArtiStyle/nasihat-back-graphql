// @flow

// $FlowFixMe
import { GQC } from 'graphql-compose';
import { UserTC } from './User';
// import { TripTC } from './Trip';
import { UagyzTC } from './Uagyz';
import { QuestionTC } from './Question';
import { BlogTC } from './Blog';

GQC.rootQuery().addFields({
  // userById: UserTC.getResolver('findById'),
  // userOne: UserTC.getResolver('findOne'),

  questionById: QuestionTC.getResolver('findById'),
  questionOne: QuestionTC.getResolver('findOne'),
  questionMany: QuestionTC.getResolver('findMany'),

  blogById: BlogTC.getResolver('findById'),
  blogOne: BlogTC.getResolver('findOne'),
  blogMany: BlogTC.getResolver('findMany'),

  userById: UserTC.getResolver('findById'),
  userOne: UserTC.getResolver('findOne'),
  userMany: UserTC.getResolver('findMany'),

  uagyzById: UagyzTC.getResolver('findById'),
  uagyzOne: UagyzTC.getResolver('findOne'),
  uagyzMany: UagyzTC.getResolver('findMany'),
});

GQC.rootMutation().addFields({
  questionCreateOne: QuestionTC.getResolver('createOne'),
  questionUpdateById: QuestionTC.getResolver('updateById'),
  questionUpdateOne: QuestionTC.getResolver('updateOne'),
  questionRemoveOne: QuestionTC.getResolver('removeOne'),
  // invalidUploadCertificate: UserTC.getResolver('uploadCertificateById'),

  blogCreateOne: BlogTC.getResolver('createOne'),
  blogUpdateById: BlogTC.getResolver('updateById'),
  blogUpdateOne: BlogTC.getResolver('updateOne'),
  blogRemoveOne: BlogTC.getResolver('removeOne'),

  uagyzCreateOne: UagyzTC.getResolver('createOne'),
  uagyzUpdateById: UagyzTC.getResolver('updateById'),
  uagyzUpdateOne: UagyzTC.getResolver('updateOne'),
  uagyzRemoveOne: UagyzTC.getResolver('removeOne'),
  //
  userCreateOne: UserTC.getResolver('createOne'),
  userUpdateById: UserTC.getResolver('updateById'),
  // tripUpdateOne: TripTC.getResolver('updateOne'),
  // tripUpdateMany: TripTC.getResolver('updateMany'),
  userRemoveOne: UserTC.getResolver('removeOne'),
  // tripRemoveMany: TripTC.getResolver('removeMany'),
});

const schema = GQC.buildSchema();

export default schema;
