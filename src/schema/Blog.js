// @flow

import mongoose, { type MongoId } from 'mongoose';
// $FlowFixMe
import composeWithMongoose, { GraphQLMongoID } from 'graphql-compose-mongoose';
import { UserTC } from './User';

import DB from './db';

export const BlogSchema = new mongoose.Schema(
  {
    typeOf: {
      required: true,
      type: String,
      enum: ['audio', 'video', 'makal'],
      description: 'тип',
    },
    typeOfBlog: {
      required: true,
      type: String,
      enum: ['Hadis', 'Akida', 'Sira', 'Sharigat'],
      description: 'Тип блога',
    },
    // views: { type: Number, description: 'Кол-во просмотров', default: 0 },
    img: { type: String, description: 'Путь картинки', required: true },
    videoLink: { type: String, description: 'Путь картинки' },
    audioLink: [
      {
        _id: false,
        name: { type: String, description: 'Название' },
        link: { type: String, description: 'Путь ' },
      },
    ],
    title: {
      kz: { type: String, description: 'Название картинки', required: true },
      ru: { type: String, description: 'Название картинки', required: true },
    },
    mainText: {
      kz: { type: String, description: 'Название картинки', required: true },
      ru: { type: String, description: 'Название картинки', required: true },
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      description: 'Автор статьи',
    },
  },
  {
    timestamps: true,
    collection: 'blogs',
  }
);
BlogSchema.index({ createdAt: 1 });

export class BlogDoc /* :: extends Mongoose$Document */ {
  typeOf: string;
  typeOfBlog: string;
  img: string;
  videoLink: string;
  audioLink: [{ name: string, link: string }];
  name: string;
  link: string;
  title: { kz: string, ru: string };
  mainText: { kz: string, ru: string };
  author: MongoId;

  static async createRecord(data: $Shape<BlogDoc>): Promise<BlogDoc> {
    console.log(data);
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<BlogDoc>): Promise<BlogDoc> {
    console.log(data);
    const blog: any = await Blog.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return blog; // const doc = new this(data);
    // return doc.save();
  }
  static async deleteRecord(data: $Shape<BlogDoc>): Promise<BlogDoc> {
    const blog = await Blog.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }

  static async getBlog(id: string): Promise<BlogDoc> {
    try {
      const user: any = await Blog.findOne({ _id: id });

      return user;
    } catch (e) {
      return e;
    }
  }
  static async getBlogs(): Promise<Array<BlogDoc>> {
    const blogs = await Blog.find();
    return blogs;
  }
  static async getBlogsByType(typeOf: string): Promise<Array<BlogDoc>> {
    const blogs = await Blog.find({ typeOf });
    return blogs;
  }
   static async addAudio(_id: string, name: string, link: string) {
    console.log(_id,name,link)
    const like: any = await Blog.findOneAndUpdate(
      { _id },
      {
        $push: {
          audioLink: { name, link },
        },
      },
      { new: true }
    ).exec();
    return like;
  }
  static async deleteAudio(_id: string, index: number): Promise<BlogDoc> {
    const blog: any = await Blog.findOne({ _id });
    blog.audioLink.splice(index, 1);
    const newBlog = await blog.save();
    return newBlog;
  }
}

BlogSchema.loadClass(BlogDoc);

export const Blog = DB.data.model('Blog', BlogSchema);

export const BlogTC = composeWithMongoose(Blog);

// BlogTC.addRelation('user', {
//   resolver: () => UserTC.getResolver('findOne'),
//   prepareArgs: {
//     filter: source => ({ userid: `${source._id}` }),
//   },
//   projection: { _id: true },
// });
// BlogTC.addResolver({
//   name: 'findManyByUserId',
//   type: [BlogTC],
//   args: {
//     id: GraphQLMongoID,
//   },
//   resolve: rp => {
//     const { source, args } = rp || {};
//     const { role } = source || {};
//     const { id } = args || {};
//     if (role === 'driver') return Blog.find({ driverId: id });
//     if (role === 'passenger' || role === 'invalid') return Blog.find({ passengerId: id });
//     return new Error('User must have role');
//   },
// });
//
// BlogTC.addResolver({
//   name: 'findManyByVehicleId',
//   type: [BlogTC],
//   args: {
//     id: GraphQLMongoID,
//   },
//   resolve: rp => {
//     const { args } = rp || {};
//     const { id } = args || {};
//     return Blog.findOne({ vehicleId: id });
//   },
// });
