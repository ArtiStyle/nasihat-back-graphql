// @flow

import mongoose, { type MongoId } from 'mongoose';
// $FlowFixMe
import composeWithMongoose, { GraphQLMongoID } from 'graphql-compose-mongoose';

import DB from './db';

export const QuestionSchema = new mongoose.Schema(
  {
    name: { type: String, description: 'Name of author' },
    email: { type: String, description: 'email' },
    theme: { type: String, description: 'theme' },
    moderated: { type: Boolean, description: 'Текст вопроса', default: false },
    textOf: { type: String, description: 'Текст вопроса' },
    answer: { type: String, description: 'Ответ вопроса', default: 'Қаралмаған' },
  },
  {
    timestamps: true,
    collection: 'questions',
  }
);

export class QuestionDoc /* :: extends Mongoose$Document */ {
  moderated: boolean;
  textOf: number;
  answer: string;

  static async createRecord(data: $Shape<QuestionDoc>): Promise<QuestionDoc> {
    console.log(data);
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<QuestionDoc>): Promise<QuestionDoc> {
    const answer: any = await Question.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer;
  }
    static async deleteRecord(data: $Shape<QuestionDoc>): Promise<QuestionDoc> {
    const blog = await Question.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }
  static async getQuestion(id: string): Promise<QuestionDoc> {
    const question: any = await Question.findOne({ _id: id });
    return question;
  }
  static async getQuestions(): Promise<Array<QuestionDoc>> {
    const Questions = await Question.find();
    return Questions;
  }
}

QuestionSchema.loadClass(QuestionDoc);

export const Question = DB.data.model('Question', QuestionSchema);

export const QuestionTC = composeWithMongoose(Question);

// QuestionTC.addResolver({
//   name: 'findManyByUserId',
//   type: [QuestionTC],
//   args: {
//     id: GraphQLMongoID,
//   },
//   resolve: rp => {
//     const { source, args } = rp || {};
//     const { role } = source || {};
//     const { id } = args || {};
//     if (role === 'driver') return Question.find({ driverId: id });
//     if (role === 'passenger' || role === 'invalid') return Question.find({ passengerId: id });
//     return new Error('User must have role');
//   },
// });
//
// QuestionTC.addResolver({
//   name: 'findManyByVehicleId',
//   type: [QuestionTC],
//   args: {
//     id: GraphQLMongoID,
//   },
//   resolve: rp => {
//     const { args } = rp || {};
//     const { id } = args || {};
//     return Question.findOne({ vehicleId: id });
//   },
// });
