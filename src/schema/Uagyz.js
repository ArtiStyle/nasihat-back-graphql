// @flow

import mongoose, { type MongoId } from 'mongoose';
// $FlowFixMe
import composeWithMongoose, { GraphQLMongoID } from 'graphql-compose-mongoose';

import DB from './db';

export const UagyzSchema = new mongoose.Schema(
  {
    title: {
      kz: { type: String, description: 'Название картинки', required: true },
      ru: { type: String, description: 'Название картинки', required: true },
    },
    mainText: {
      kz: { type: String, description: 'Название картинки', required: true },
      ru: { type: String, description: 'Название картинки', required: true },
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      description: 'Автор статьи',
    },
  },
  {
    timestamps: true,
    collection: 'uagyzes',
  }
);

export class UagyzDoc /* :: extends Mongoose$Document */ {
  title: { kz: string, ru: string };
  mainText: { kz: string, ru: string };
  author: MongoId;

  static async createRecord(data: $Shape<UagyzDoc>): Promise<UagyzDoc> {
    console.log(data);
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<UagyzDoc>): Promise<UagyzDoc> {
    const answer: any = await Uagyz.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer; // const doc = new this(data);
    // return doc.save();
  }
  static async getUagyzs(): Promise<Array<UagyzDoc>> {
    const blogs = await Uagyz.find().populate({
      path: 'author',
    });;
    return blogs;
  }
  static async getUagyz(id: string): Promise<UagyzDoc> {
    const question: any = await Uagyz.findOne({ _id: id }).populate({
      path: 'author',
      select: 'name avatar',
    });;;
    return question;
  }
  static async deleteRecord(data: $Shape<UagyzDoc>): Promise<UagyzDoc> {
    const blog = await Uagyz.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }
}

UagyzSchema.loadClass(UagyzDoc);

export const Uagyz = DB.data.model('Uagyz', UagyzSchema);

export const UagyzTC = composeWithMongoose(Uagyz);

UagyzTC.addResolver({
  name: 'findOne',
  type: [UagyzTC],
  args: {
    id: GraphQLMongoID,
  },
  resolve: () => {
    return Uagyz.find().populate({
      path: 'author',
    });
  },
});
UagyzTC.addResolver({
  name: 'findMany',
  type: [UagyzTC],
  args: {
    id: GraphQLMongoID,
  },
  resolve: () => {
    return Uagyz.find().populate({
      path: 'author',
      select: 'name avatar',
    });
  },
});

// UagyzTC.addResolver({
//   name: 'findManyByVehicleId',
//   type: [UagyzTC],
//   args: {
//     id: GraphQLMongoID,
//   },
//   resolve: rp => {
//     const { args } = rp || {};
//     const { id } = args || {};
//     return Uagyz.findOne({ vehicleId: id });
//   },
// });
