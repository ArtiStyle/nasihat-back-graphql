import express from 'express';
import multer from 'multer';
import fs from 'fs';

import { User } from '../schema/User';
import { Blog } from '../schema/Blog';

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/img');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});
const upload = multer({ storage });

const router = express.Router();

router.get('/get/:id', async (req, res) => {
  console.log('tut');
  try {
    const blog = await Blog.getBlog(req.params.id);
    // const comments = await Comment.getComments(req.params.id);
    res.send({ blog });
  } catch (e) {
    res.send(e);
  }
});
router.get('/bytype/:type', async (req, res) => {
  console.log('tut');
  try {
    const blog = await Blog.getBlogsByType(req.params.type);
    res.send(blog);
  } catch (e) {
    res.send(e);
  }
});

// router.post('/addaudio', upload.any(), async (req, res) => {
//   console.log(req.body);
//   const { files, body } = req || {};
//   const { typeOf, typeOfBlog, audio, titlekz, titleru, mainTextkz, mainTextru, author } = body;
//   // const { title, mainText, author, _id, VideoLink, audioLink } = body || {};
//   // const { path: pathToFile } = files.avatar[0] || {};
//   const obj = {
//     typeOf,
//     typeOfBlog,
//     title: {
//       kz: titlekz,
//       ru: titleru,
//     },
//     mainText: {
//       kz: mainTextkz,
//       ru: mainTextru,
//     },
//     author,
//   };
//   obj.audioLink = [];
//   let fi = 0;
//   if (req.body.audio) {
//     for (let i = 0; i < audio.length; i++) {
//       if (req.body.audio[i].link) {
//         obj.audioLink.push({ name: audio[i].name, link: audio[i].link });
//       } else {
//         obj.audioLink.push({ name: audio[i].name, link: files[fi].path });
//         fi++;
//       }
//     }
//   }
//   if (files) {
//     for (const i of files) {
//       if (i.fieldname === 'img') {
//         obj.img = i.path;
//       }
//     }
//   }
//   console.log(obj);
//   try {
//     console.log(obj);
//     const blog = await Blog.createRecord(obj);
//     res.send(blog);
//   } catch (e) {
//     res.send(e);
//   }
// });

router.post('/add', upload.any(), async (req, res) => {
  console.log(req.body);
  const { files, body } = req || {};
  const {
    typeOf,
    typeOfBlog,
    VideoLink,
    audio,
    titlekz,
    titleru,
    mainTextkz,
    mainTextru,
    author,
  } = body;
  // const { title, mainText, author, _id, VideoLink, audioLink } = body || {};
  // const { path: pathToFile } = files.avatar[0] || {};
  const obj = {
    typeOf,
    typeOfBlog,
    VideoLink,
    title: {
      kz: titlekz,
      ru: titleru,
    },
    mainText: {
      kz: mainTextkz,
      ru: mainTextru,
    },
    author,
  };
  obj.audioLink = [];
  let fi = 0;
  if (req.body.audio) {
    for (let i = 0; i < audio.length; i++) {
      if (req.body.audio[i].link) {
        obj.audioLink.push({ name: audio[i].name, link: audio[i].link });
      } else {
        obj.audioLink.push({ name: audio[i].name, link: files[fi].path });
        fi++;
      }
    }
  }
  if (files) {
    for (const i of files) {
      if (i.fieldname === 'img') {
        obj.img = i.path;
      }
    }
  }
  console.log(obj);
  try {
    console.log(obj);
    const blog = await Blog.createRecord(obj);
    res.send(blog);
  } catch (e) {
    res.send(e);
  }
});

router.post('/addmakal', upload.single('img'), async (req, res) => {
  console.log(req.body);
  const { file, body } = req || {};
  const { path } = file || {};

  const { typeOf, typeOfBlog, titlekz, titleru, mainTextkz, mainTextru, author } = body;
  const obj = {
    typeOf,
    typeOfBlog,
    img: path,
    title: {
      kz: titlekz,
      ru: titleru,
    },
    mainText: {
      kz: mainTextkz,
      ru: mainTextru,
    },
    author,
  };
  console.log(obj);
  try {
    const blog = await Blog.createRecord(obj);
    res.send(blog);
  } catch (e) {
    res.send(e);
  }
});

router.post('/edit', upload.single('avatar'), async (req, res) => {
  const { file, body } = req || {};
  console.log(body);
  // const { title, mainText, author, _id, VideoLink, audioLink } = body || {};
  const { path: pathToFile } = file || {};
  const obj = body;
  if (pathToFile) {
    obj.img = pathToFile;
    console.log('in deleting');
    const blogBefore = await Blog.getBlog(body._id);
    fs.unlink(blogBefore.img, err => {
      if (err) throw err;
      console.log('successfully deleted ');
    });
  }
  console.log(body);
  try {
    const blog = await Blog.editRecord(body);
    res.send(blog);
  } catch (e) {
    res.send(e);
  }
});

router.post('/addAudio', upload.single('audio'), async (req, res) => {
  const { _id, name } = req.body;
  const { file } = req || {};
  const { path: pathToFile } = file || {};
  console.log(pathToFile,_id,name)
  const blog = await Blog.addAudio(_id, name, pathToFile);
  res.send(blog);
});

router.post('/deleteAudio', async (req, res) => {
  const { _id, index, path } = req.body;
  fs.unlink(path, err => {
    if (err) throw err;
    console.log('successfully deleted ');
  });
  const blog = await Blog.deleteAudio(_id, index);
  res.send(blog);
});

router.post('/delete', async (req, res) => {
  const { _id } = req.body;
  try {
    const blog = await Blog.getBlog(_id);
    // if (blog.audioLink.length > 0) {
    //   for (const i of blog.audioLink) {
    //     fs.unlink(i.link, err => {
    //       if (err) throw err;
    //       console.log('successfully deleted ');
    //     });
    //   }
    // }
    console.log('in deleting');
    // fs.unlink(blog.img, err => {
    //   if (err) throw err;
    //   console.log('successfully deleted ');
    // });
    const makal = await Blog.deleteRecord(_id);
    res.send(makal);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
