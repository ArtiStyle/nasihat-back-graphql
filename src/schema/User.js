// @flow

import mongoose from 'mongoose';
import type { $Request } from 'express';
// $FlowFixMe
import { composeWithMongoose } from 'graphql-compose-mongoose';
import { BlogTC } from './Blog';
import DB from './db';

export const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      // required: true,
      description: 'Имя пользователя',
    },
    avatar: {
      type: String,
      description: 'Avatar пользователя',
    },
    biography: {
      kz: { type: String, description: 'Название картинки', required: true },
      ru: { type: String, description: 'Название картинки', required: true },
    },
  },
  {
    timestamps: true,
    collection: 'users',
  }
);

export class UserDoc /* :: extends Mongoose$Document */ {
  name: string;
  avatar: string;
  biography: { kz: string, ru: string };

  static async createRecord(data: $Shape<UserDoc>): Promise<UserDoc> {
    console.log(data);
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<UserDoc>): Promise<UserDoc> {
    console.log(data);

    const user: any = await User.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return user.save();
  }
  static async deleteRecord(data: $Shape<UserDoc>): Promise<UserDoc> {
    const blog = await User.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }

  static async getUsers(): Promise<Array<UserDoc>> {
    let users = [];
    users = await User.find();
    return users;
  }
  static async getUser(_id: string): Promise<UserDoc> {
    const user: any = await User.findOne({ _id });
    return user;
  }
  // static isValidPhone(phone: string): boolean {
  //   console.log(phone);
  //   return true;
  // }
}

UserSchema.loadClass(UserDoc);

export const User = DB.data.model('User', UserSchema);

export const UserTC = composeWithMongoose(User);

UserTC.addRelation('blog', {
  resolver: () => BlogTC.getResolver('findById'),
  prepareArgs: {
    id: source => source._id,
  },
});
// UserTC.addRelation('uagyz', {
//   resolver: () => BlogTC.getResolver('findById'),
//   prepareArgs: {
//     id: source => source._id,
//   },
// });

// UserTC.addRelation('trips', {
//   resolver: () => TripTC.getResolver('findManyByUserId'),
//   prepareArgs: {
//     id: source => source._id,
//   },
//   projection: {
//     role: true,
//   },
// });

// UserTC.addResolver({
//   kind: 'mutation',
//   name: 'uploadCertificateById',
//   type: UserTC,
//   args: {
//     id: GraphQLMongoID,
//     file: Object,
//   },
//   resolve: async rp => {
//     const { args } = rp || {};
//     const { id, file } = args || {};
//     const user = await User.findOne({ _id: id });
//     if (!user) throw new Error(`No user found with this id: ${id}`);
//     if (user && user.role !== 'invalid')
//       throw new Error('Only invalids can upload the certificate');
//   },
// });
