import express from 'express';

import { Question } from '../schema/Question';

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const questions = await Question.getQuestions();
    res.send(questions);
  } catch (e) {
    res.send(e);
  }
});
router.get('/:id', async (req, res) => {
  try {
    const question = await Question.getQuestion(req.params.id);
    res.send(question);
  } catch (e) {
    res.send(e);
  }
});
router.post('/', async (req, res) => {
  const { body } = req;
  try {
    const question = await Question.createRecord(body);
    res.send(question);
  } catch (e) {
    res.send(e);
  }
});
router.post('/answer', async (req, res) => {
  const { body } = req || {};
  const { _id, answer } = body || {};
  try {
    const question = await Question.editRecord({
      _id,
      answer,
    });
    res.send(question);
  } catch (e) {
    res.send(e);
  }
});

router.post('/delete', async (req, res) => {
  const { _id } = req.body;
  try {
    const makal = await Question.deleteRecord(_id);
    res.send(makal);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
